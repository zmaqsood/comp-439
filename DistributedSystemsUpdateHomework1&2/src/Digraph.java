import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;

public class Digraph {

	public final HashSet<Task> GraphTasks;
	public HashSet<Task> StartTasks;// = new HashSet<Task>(); 
	
	public Digraph(){
		GraphTasks = new HashSet<Task>();
		StartTasks = new HashSet<Task>();
	}
	
	public Task addTask(Task T){
		GraphTasks.add(T);
		return T;
	}
	
	public int getNumTask(){
		return GraphTasks.size();
	}
	
	
	public void StartTasks(Task[] AllTasks){
		
		for(Task n : AllTasks){
		      if(n.inEdges.size() == 0){
		        StartTasks.add(n);
		      }
		    }
	}
	
	public boolean check(Task t){
		return (GraphTasks.contains(t));
	}
		
	public static void main(String[] args){
	Task one = new Task("1", "Sleep");
	Task two = new Task("2","Sleep");
	Task three = new Task("3","sum");
	Task four = new Task("4", "Sleep");
	Task five = new Task("5","Sleep");
	Task six = new Task("6","Sleep");
	Task seven = new Task("7","sum");
	Task eight = new Task("8", "sum");
	    
	one.addEdge(three);
	two.addEdge(four);
	four.addEdge(three);
	three.addEdge(five).addEdge(six);
	five.addEdge(seven);
	six.addEdge(seven);
	one.addEdge(four);	
    
	Task[] allTasks = {one, two, three, four, five, six, seven, eight};
	
    Digraph G = new Digraph();
    
    //adding tasks to graph
    for(Task T : allTasks){
    	G.addTask(T);
    }
        
    //fetching the start tasks in graph(tasks that don't have incoming edges)
    G.StartTasks(allTasks);
            
    //Arraylist that will have the sorted tasks
    ArrayList<Task> outList = new ArrayList<Task>();
    
    //Arraylist to temperory hold the tasks for execution
    ArrayList<Task> templist = new ArrayList<Task>();
    
   long totaltime = 0;
   
   long timeStart = System.currentTimeMillis(); //time taken by threads
   
   while(!G.StartTasks.isEmpty() || (!templist.isEmpty())){
    
    if(!G.StartTasks.isEmpty()){
    	
      Task T = G.StartTasks.iterator().next();
      G.StartTasks.remove(T);

      //insert T into output list
      outList.add(T);
      templist.add(T);
      T.myThread.start();
    }

    for(int i = 0 ; i < templist.size(); i++){
    	  if(templist.get(i).getFinished()){
    		  
    		  //for each Task to with an edge e from "T" to "to" do
    		  for(Iterator<Edge> iterator = templist.get(i).outEdges.iterator();iterator.hasNext();){
    			  
    			  //remove edge e from form outEdges of T
    			  Edge e = iterator.next();
    			  Task to = e.to;
    			  iterator.remove();//Remove edge from T (by removing it from outEdges list of T)
    			  to.inEdges.remove(e);//Remove edge from to(by removing it from inEdges list of to)
        
    			  //if to has no other incoming edges then insert it into StartTasks
    			  if(to.inEdges.isEmpty()){
    				  //System.out.println("Added trask: " + to.name);
    				  G.StartTasks.add(to);
    			  }
    		  }
    		  templist.remove(i);
    	  } 
      }
    }
   
   long timeStop = System.currentTimeMillis();
   totaltime += timeStop - timeStart;
    
    //Check to see if all edges are removed
    boolean cycle = false;
    for(Task T : allTasks){
      if(!T.inEdges.isEmpty()){
        cycle = true;
        break;
      }
    }
    if(cycle){
      System.out.println("There's a cycle in graph!");
    }else{
      System.out.println("No cycle, Tasks execution order "+Arrays.toString(outList.toArray()));
    }
  }
}
	

