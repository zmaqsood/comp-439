import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.HashSet;

import junit.framework.Assert;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;


public class DigraphTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		Digraph G = new Digraph();
		assertEquals(G.getNumTask(),0);//hashset size = 0
	}

	@After
	public void tearDown() throws Exception {
	}
	
	//Test to see if tasks are created
	@Test public void TaskCreation(){
		Task T1 = new Task("T1","Sleep");
		Task T2 = new Task("T2","Sleep");
		Task T3 = new Task("T3","sum");
		
		assertEquals(T1.name, "T1");
		assertEquals(T1.job, "Sleep");

		assertEquals(T2.name, "T2");
		
		assertEquals(T2.job, "Sleep");
		assertNotSame(T2.job, "sum");

		assertTrue(T1.equals(T1));
		assertTrue(T2.equals(T2));
	}
	
	//Test Digraph size to check if the task is added?
	@Test public void addTask(){
		Digraph G = new Digraph();
		G.addTask(new Task("T1","Sleep"));
		G.addTask(new Task("T1", "Sleep"));
		assertEquals(G.getNumTask(),2);//total tasks is 2
		assertFalse(G.GraphTasks.contains(new Task("T1","Sleep")));

	}
	
		
	//Test to see if a given task is in the graph?
	@Test
	public void CheckSpecificTask(){
		Digraph G = new Digraph();
		Task T1 = G.addTask(new Task("T1","Sleep"));
		G.addTask((new Task("T1", "Sleep")));
		Task T2 = G.addTask(new Task("T2","Sum"));
		Task T3 = G.addTask(new Task("T3","Sleep"));

		assertFalse(G.check(new Task("T1","Sleep")));//task created but not added to graph
		assertTrue(G.check(T1)); //created and added
		assertTrue(G.check(T2));
		assertTrue(G.check(T3));
	}
	
	//Test to see if an edge has been added?
	@Test
	public void TestAddEdge(){
		Task T2 = new Task("T2","Sleep");
		Task T3 = new Task("T3","sum");
		
		assertEquals(T2.name, "T2");
		assertEquals(T3.name, "T3");
		
		Edge edge = new Edge(T2,T3);
		
		assertEquals(edge.from, T2);//Assert.assertEquals(T2.equals(T2));
		assertEquals(edge.to, T3);
		
		assertTrue(T2.outEdges.add(edge));
		assertTrue(T3.inEdges.add(edge));
		
	}
	
	//Test to see the startnodes
	@Test
	public void StartNodes(){
		HashSet<Task> S = new HashSet<Task>();
		ArrayList<Task> TaskList = new ArrayList<Task>();
		
		Task T1 = new Task("T1","Sleep");
		Task T5 = new Task("T5","sum");
		Task T2 = new Task("T2","sum");
		
		TaskList.add(T1);
		TaskList.add(T5);
		TaskList.add(T2);
		
		Edge edge = new Edge(T1,T5);
		assertTrue(T1.outEdges.add(edge));
		assertTrue(T5.inEdges.add(edge));
		
		Edge edge2 = new Edge(T5,T2);
		assertTrue(T5.outEdges.add(edge2));
		
		for(Task T : TaskList){
			if(T.inEdges.size() == 0){
				S.add(T);
			}
		}
		
		assertEquals(S.size(),2);
	}
	
		
	

}
