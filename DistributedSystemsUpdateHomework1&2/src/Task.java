import java.util.HashSet;

public class Task implements Runnable {
	private String name;
	protected String job;
    public final HashSet<Edge> inEdges;
    public final HashSet<Edge> outEdges;
    Thread myThread;
    public boolean finished = false;
       
    public Task(String S, String key){
		this.name = S;
		this.job = key;
		inEdges = new HashSet<Edge>();
		outEdges = new HashSet<Edge>();
		myThread = new Thread(this);
	}
   
    public Task addEdge(Task task){
        Edge e = new Edge(this, task);
        outEdges.add(e);
        task.inEdges.add(e);
        return this;
      }
             
    @Override
    public boolean equals(Object obj) {
      Task x = (Task)obj;
      return ( x.name == this.name);
    }
    
    @Override
    public String toString() {
      return name;
    }

	@Override
	public void run() {
		
			if (this.job == "Sleep"){
				System.out.println("Starting Task " + this.name );
				int x = 5 + (int)(Math.random() * ((10 - 5) + 1));
	            try {
	            	Thread.sleep((int) x * 100);
	            	} catch (InterruptedException e) {}
				}
			else {
				int result = 0;
				System.out.println("Starting Task " + this.name);
				for (int i = 0 ; i < (int) Math.random() * 1000000000 ; i++){
					result += i;
					}
	    	}System.out.println("Finished Task:" + this.name + " DONE! ");
	    	finished = true;
	}
	
	public boolean getFinished(){
		return finished;
	}
}
