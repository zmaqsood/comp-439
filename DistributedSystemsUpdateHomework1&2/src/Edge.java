
public class Edge {
	protected Task from;
	protected Task to;
	public Edge(Task from, Task to){
		this.from = from;
		this.to = to;
	}
	@Override
	public boolean equals(Object O){
		Edge e = (Edge) O;
		return (e.from == from && e.to == to);
	}
}
